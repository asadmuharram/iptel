<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>IP Telephony</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<!-- CuFon: Enables smooth pretty custom font rendering. 100% SEO friendly. To disable, remove this section -->
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<!-- CuFon ends -->

<script src="js/jquery.js" type="text/javascript"></script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
     <?php include("inc/menutop.php"); ?>
      <div class="logodua"><h4></h4></div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="body">
    <div class="body_resize">
      <div class="left"> 
	   <?php include("inc/video.php"); ?>
      </div>
      <div class="right">
       <?php include("inc/menuright-vicon.php"); ?> 
      </div>
        <div class="clr"></div>
    </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="FBG">
    <div class="FBG_resize">
     
      <div class="clr"></div>
    </div>
  </div>
  <?php include("inc/footer.php"); ?> 
</div>
</body>
</html>
