 <img src="images/avaya9608.jpg" alt="img" width="230" height="222"  />
        <h2>Avaya <span>9608</span> IP Phone</h2>

        <p><strong>Manual Guide :</strong><br></p>
			<p><a href="files/avaya9608_a4_en_v1.pdf"><img src="images/pdf.gif" class="floated"> Avaya 9608 English</a> </p><p><a href="files/avaya9608_a4_id_v1.pdf"><img src="images/pdf.gif" class="floated"> Avaya 9608 Bahasa Indonesia</a></p>
		    

<div class="bg"></div> 

	   <img src="images/avaya9650.jpg" alt="img" width="230" height="222"  />
        <h2>Avaya <span>9650</span> IP Phone</h2>

        <p><strong>Manual Guide :</strong><br>
			<p><a href="files/Avaya%209650_A4_Eng_v1.pdf"><img src="images/pdf.gif" class="floated"> Avaya 9650 English</a> </p><p><a href="files/Avaya%209650_A4_Ind_v1.pdf"><img src="images/pdf.gif" class="floated"> Avaya 9650 Bahasa Indonesia</a></p>
		    
		 <div class="bg"></div> 
        <img src="images/avaya9620.jpg" alt="img" width="230" height="230"  />
        <h2>Avaya <span>9620</span> IP Phone</h2>

        <p><strong>Manual Guide :</strong><br></p>
			<p><a href="files/Avaya%209620_A4_Eng_v1.pdf"><img src="images/pdf.gif" class="floated"> Avaya 9620 English</a></p><p><a href="files/Avaya%209620_A4_Ind_v1.pdf"><img src="images/pdf.gif" class="floated"> Avaya 9620 Bahasa Indonesia</a></p>
		    
		 <div class="bg"></div> 
        <img src="images/avaya1608.jpg" alt="img" width="230" height="230"  />
        <h2>Avaya <span>1608</span> IP Phone</h2>

        <p><strong>Manual Guide :</strong></p>
			<p><a href="files/Avaya%201608_A4_Eng_v1.pdf"><img src="images/pdf.gif" class="floated"> Avaya 1608 English</a></p><p><a href="files/Avaya%201608_A4_Ind_v1.pdf"><img src="images/pdf.gif" class="floated"> Avaya 1608 Bahasa Indonesia</a></p>
		    
