
<style type="text/css">

/* Base Styles */
#cssmenu,
#cssmenu ul,
#cssmenu li,
#cssmenu a {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  font-weight: normal;
  text-decoration: none;
  line-height: 1;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  position: relative;
}
#cssmenu {
  width: 250px;
  border-bottom: 4px solid #656659;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
}
#cssmenu a {
  line-height: 1.3;
}
#cssmenu > ul > li:first-child {
  background: #66665e;
  background: -moz-linear-gradient(#66665e 0%, #45463d 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #66665e), color-stop(100%, #45463d));
  background: -webkit-linear-gradient(#66665e 0%, #45463d 100%);
  background: linear-gradient(#66665e 0%, #45463d 100%);
  border: 1px solid #45463d;
  -webkit-border-radius: 3px 3px 0 0;
  -moz-border-radius: 3px 3px 0 0;
  border-radius: 3px 3px 0 0;
}
#cssmenu > ul > li:first-child > a {
  padding: 15px 10px;
  border: none;
  border-top: 1px solid #818176;
  -webkit-border-radius: 3px 3px 0 0;
  -moz-border-radius: 3px 3px 0 0;
  border-radius: 3px 3px 0 0;
  font-family: 'Ubuntu', sans-serif;
  text-align: center;
  font-size: 18px;
  font-weight: 300;
  text-shadow: 0 -1px 1px #000000;
}
#cssmenu > ul > li:first-child > a > span {
  padding: 0;
}
#cssmenu > ul > li:first-child:hover {
  background: #66665e;
  background: -moz-linear-gradient(#66665e 0%, #45463d 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #66665e), color-stop(100%, #45463d));
  background: -webkit-linear-gradient(#66665e 0%, #45463d 100%);
  background: linear-gradient(#66665e 0%, #45463d 100%);
}
#cssmenu > ul > li {
 
background: #ff920a;
background: -moz-linear-gradient(45deg, rgba(255,175,75,1) 0%, rgba(255,146,10,1) 100%);
background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(255,175,75,1)), color-stop(100%, rgba(255,146,10,1)));
background: -webkit-linear-gradient(45deg, rgba(255,175,75,1) 0%, rgba(255,146,10,1) 100%);
background: -o-linear-gradient(45deg, rgba(255,175,75,1) 0%, rgba(255,146,10,1) 100%);
background: -ms-linear-gradient(45deg, rgba(255,175,75,1) 0%, rgba(255,146,10,1) 100%);
background: linear-gradient(45deg, rgba(255,175,75,1) 0%, rgba(255,146,10,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffaf4b', endColorstr='#ff920a', GradientType=1 );

}
#cssmenu > ul > li:hover {
  background: #e84323;
  background: -moz-linear-gradient(#e84323 0%, #c33115 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #e84323), color-stop(100%, #c33115));
  background: -webkit-linear-gradient(#e84323 0%, #c33115 100%);
  background: linear-gradient(#e84323 0%, #c33115 100%);
}
#cssmenu > ul > li > a {
  font-size: 14px;
  display: block;
  color: #820000;
  border: 1px solid #ba2f14;
  border-top: none;
}
#cssmenu > ul > li > a > span {
  display: block;
  padding: 12px 10px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
}
#cssmenu > ul > li > a:hover {
  text-decoration: none;
  color:#FFFFFF;
}
#cssmenu > ul > li.active {
  border-bottom: none;
}
#cssmenu > ul > li.has-sub > a span {
  /* background: url(images/icon_plus.png) 96% center no-repeat;*/
}
#cssmenu > ul > li.has-sub.active > a span {
  background: url(images/icon_minus.png) 96% center no-repeat;
}
/* Sub menu */
#cssmenu ul ul {
  display: none;
  background: #fff;
  border-right: 1px solid #a2a194;
  border-left: 1px solid #a2a194;
}
#cssmenu ul ul li {
  padding: 0;
  border-bottom: 1px solid #d4d4d4;
  border-top: none;
  background: #f7f7f7;
  background: -moz-linear-gradient(#f7f7f7 0%, #ececec 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #f7f7f7), color-stop(100%, #ececec));
  background: -webkit-linear-gradient(#f7f7f7 0%, #ececec 100%);
  background: linear-gradient(#f7f7f7 0%, #ececec 100%);
}
#cssmenu ul ul li:last-child {
  border-bottom: none;
}
#cssmenu ul ul a {
  padding: 10px 10px 10px 25px;
  display: block;
  color: #676767;
  font-size: 12px;
  font-weight: normal;
}
#cssmenu ul ul a:before {
  content: "»";
  position: absolute;
  left: 10px;
  color: #e94f31;
}
#cssmenu ul ul a:hover {
  color: #e94f31;
}

</style>   

   <title>Menu</title>
</head>
<body>

<div id='cssmenu'>
<ul>
  <li></li>
   <li class='has-sub'><a href="ext.php"><span><b>Daftar No. Ext</b><br><i>Ext. No List</i></span></a></li>
   <li class='has-sub'><a href="tips.php"><span><b>Tips Menghemat Biaya Telpon</b> <br><i>Phone Cost Saving Tips</i></span></a></li>
   <li class='has-sub'><a href="avaya1.php"><span><b>Jenis Pesawat Telpon Avaya</b><br><i>Avaya Phone Type</i></span></a></li>
   <li class='has-sub'><a href="tab.php"><span><b>Daftar kantor yg terpasang dgn system IP Telephony<br></b><i>List of offices which have been installed with IP Telephony system</i></span></a></li>
   <li class='last'><a href="soft.php"><span><b>Softphone</b></span></a></li>
</ul>
</div><br>

			<img src="images/hotline_82001.png"  >
       
</ul>
