<style>
table.contacts
{ width: 672px;
background-color: #fafafa;
border: 1px #000000 solid;
border-collapse: collapse;
border-spacing: 0px; }


td.contactDept
{ background-color: #99CCCC;
border: 1px #000000 solid;
font-family: Verdana;
font-weight: bold;
font-size: 12px;
text-align:center;
color: #404040; }


td.contact
{ border-bottom: 1px #6699CC dotted;
text-align: left;
font-family: Verdana, sans-serif, Arial;
font-weight: normal;
font-size: .7em;
color: #404040;
background-color: #fafafa;
padding-top: 4px;
padding-bottom: 4px;
padding-left: 8px;
padding-right: 0px; }

td.contact1
{ border-bottom: 1px #6699CC dotted;
text-align: left;
font-family: Verdana, sans-serif, Arial;
font-weight: normal;
font-size: .7em;
color: #404040;
background-color: #CDEDED;
padding-top: 4px;
padding-bottom: 4px;
padding-left: 8px;
padding-right: 0px; }

</style>
<h2>Daftar Kantor yang terpasang dengan sistem IP Telephony</h2>
		  <br>
<table class="contacts" cellspacing="0" summary="Contacts template">
<tr>

</tr>
<tr>
  <td class="contactDept" ><strong>No</strong></td>
  <td class="contactDept"><strong>Nama Kantor</strong></td>
  <td class="contactDept"><strong>Telepon</strong></td>
  <td class="contactDept"><strong>Ext. Operator</strong></td>
  <td class="contactDept"><strong>Ext. Fax</strong></td>
</tr>
<tr>
<td width="4%" rowspan="5" class="contact">1</td>
<td width="23%" rowspan="5" class="contact"><strong>WISMA MILLENIA <br>
  - JCI, <br>
  - 
  MBAI, <br>
  - 
  Santori, <br>
  -
  PKP, <br>
  - 
  STP</strong></td>
<td class="contact" width="17%">021-28545680, </td>
<td class="contact" width="17%">10700, 10008, 10800, 10500 - JCI</td>
<td class="contact" width="39%">Fax JCI (10012 JCI LT.7, 10251 LH, 10300 CPM, 10400 Sec Lanny, 10508 Melianawati, 10510  Nutrisi, 10555 Mailbox LT.5 , 10541  JCI Trading LT.5</td>
</tr>
<tr>
<td class="contact">021-28545780, </td>
<td class="contact">46000, 46002, 46003 MBAI</td>
<td class="contact">Fax MBAI (46006-46007 Umum, 46008 Logistik, 46009 Marketting, 46010 Teguh)</td>
</tr>
<tr>
<td class="contact">021-28545880, </td>
<td class="contact">52001, 52002, 52007 PKP</td>
<td class="contact">Fax PKP (52008 Scret. Dewi, 52009 Scret. Agnes, 52010 Scret. Diah, 52012-52017 Umum, 52013 Logistik, 52014 PKP Pulet, 52015 RPA, 52016 Yayasan.</td></tr>
<tr>
  <td class="contact">021-29260920, </td>
  <td class="contact">64150, 64151  Santori</td>
  <td class="contact">Fax Santori (64100 Umum, 64101 Acounting, 69014 Greenfields)</td>
</tr>
<tr>
  <td class="contact">&nbsp;</td>
  <td class="contact">&nbsp;</td>
  <td class="contact">Fax STP (56100 Cold Storage, 56143 Tambak) </td>
</tr>
<tr>
  <td rowspan="4" class="contact1">2</td>
  <td rowspan="4" class="contact1"><strong>DAAN MOGOT <br>
    - JCI, <br>
    - SGF, <br>
    - AAF</strong></td>
  <td class="contact1">021-25617010</td>
  <td class="contact1">11000 - JCI</td>
  <td class="contact1">Fax JCI (11132 JCI , 11133 Corporate Development, 11299 Litigasi)</td>
</tr>
<tr>
  <td class="contact1">021-25617110</td>
  <td class="contact1">59000 - SGF HO</td>
  <td class="contact1">Fax SGF HO 59090</td>
</tr>
<tr>
  <td class="contact1">021-25617170</td>
  <td class="contact1">59200 - SGF Unit</td>
  <td class="contact1">Fax SGF Unit  59259</td>
</tr>
<tr>
  <td class="contact1"> 021-25617181</td>
  <td class="contact1">62012 - Austasia Food</td>
  <td class="contact1">Fax Austasia Food 62020</td>
</tr>
<tr>
  <td rowspan="3" class="contact">3</td>
  <td rowspan="3" class="contact"><strong>JCI SIDOARJO <br>
    - JCI, <br>
    - MBAI, <br>
    - STP, <br>
    -
    Ciomas</strong></td>
  <td class="contact"> 031 - 2988333</td>
  <td class="contact">11300 - JCI</td>
  <td class="contact">Fax JCI (11356 Sales, 11357 Koperasi,11358,11359 Pembelian, 11360 Tina, 11436 Diana, 11437 Angiline, 11438 Emil, 11439 Ida, 11440 Lanny, 11441 Legal, 11550 Herman, 11551 Melani, 11552 Nanik, 11678 Djoni, )</td>
</tr>
<tr>
  <td class="contact"> 031 - 2988345</td>
  <td class="contact">46300 - MBAI</td>
  <td class="contact">Fax MBAI (46367 HRD, 46368 Marketing, 46369 Logistik, 46370 Finance)</td>
</tr>
<tr>
  <td class="contact"> 031-2988488</td>
  <td class="contact">56300 - STP</td>
  <td class="contact">Fax STP (56265 Marketting, 56266 Purchasing, 56267 Umum, 56268 F&amp;A, 56269 HRD, GA)</td>
</tr>
<tr>
  <td class="contact1">4</td>
  <td class="contact1"><strong>JCI GEDANGAN SIDOARJO</strong></td>
  <td class="contact1"> 031-8913612</td>
  <td class="contact1">29004</td>
  <td class="contact1">29077 HRD, 29078 PEMBELIAN, 29079 MARKETTING</td>
</tr>
<tr>
  <td rowspan="2" class="contact">5</td>
  <td rowspan="2" class="contact"><strong>JCI LAMPUNG / <br>
    AUSTASIA STOCKFEED LPG</strong></td>
  <td rowspan="2" class="contact"> 0721-350149</td>
  <td class="contact">11800, 18001 - JCI</td>
  <td class="contact">11806, 11867</td>
</tr>
<tr>
  <td class="contact">33000 - AUSTASIA</td>
  <td class="contact">11806</td>
</tr>
<tr>
  <td class="contact1">6</td>
  <td class="contact1"><strong>JCI NILAM</strong></td>
  <td class="contact1"> 031-3291897</td>
  <td class="contact1">71000</td>
  <td class="contact1">71298</td>
</tr>
<tr>
  <td class="contact">7</td>
  <td class="contact"><strong>JCI MAKASSAR</strong></td>
  <td class="contact"> 0411-553494</td>
  <td class="contact">13001</td>
  <td class="contact">13030 F&amp;A, 13031 Marketting</td>
</tr>
<tr>
  <td class="contact1">8</td>
  <td class="contact1"><strong>JCI CIREBON</strong></td>
  <td class="contact1"> 0231-204507 (JCI),  0231-203058 (STP)</td>
  <td class="contact1">12100 - JCI, 12200 STP</td>
  <td class="contact1">JCI 12411, Ciomas 12412, Umum 12413, STP 12414, Sales 12415</td>
</tr>
<tr>
  <td class="contact">9</td>
  <td class="contact"><strong>JCI TANGERANG</strong></td>
  <td class="contact"> 021-5961888</td>
  <td class="contact">12661<span class="contact1"> - </span>12662</td>
  <td class="contact">12667 Cecillia, 12668 Dewi, 12669 Procurement</td>
</tr>
<tr>
  <td class="contact1">10</td>
  <td class="contact1"><strong>JCI MARGOMULYO</strong></td>
  <td class="contact1"> 031-7490601</td>
  <td class="contact1">29803</td>
  <td class="contact1">29807 Umum, 29865 Acounting</td>
</tr>
<tr>
  <td class="contact">11</td>
  <td class="contact"><strong>JCI CIKANDE</strong></td>
  <td class="contact"> 0254-403240</td>
  <td class="contact">29500</td>
  <td class="contact">29515 Marketting, 29710 QC, 29760 Umum</td>
</tr>
<tr>
  <td class="contact1">12</td>
  <td class="contact1"><strong>JCI BANJARMASIN</strong></td>
  <td class="contact1"> 0511-7496899</td>
  <td class="contact1">26100</td>
  <td class="contact1">26125</td>
</tr>
<tr>
  <td class="contact">13</td>
  <td class="contact"><strong>JCI SRAGEN</strong></td>
  <td class="contact"> 0271-890609</td>
  <td class="contact">25000</td>
  <td class="contact">25004, 25005</td>
</tr>
<tr>
  <td class="contact1">14</td>
  <td class="contact1"><strong>JCI PADANG</strong></td>
  <td class="contact1"> 0751-484595</td>
  <td class="contact1">25900</td>
  <td class="contact1">25545 HOU, 25546 Sales, 25547 Procurement</td>
</tr>
<tr>
  <td class="contact">15</td>
  <td class="contact"><strong>JCI WONOAYU <br>
    - Karung Plastik</strong></td>
  <td class="contact"> 031-8972876</td>
  <td class="contact">71400</td>
  <td class="contact">71355</td>
</tr>
<tr>
  <td class="contact1">16</td>
  <td class="contact1"><strong>INDOJAYA MEDAN</strong></td>
  <td class="contact1"> 061-7940211</td>
  <td class="contact1">32100</td>
  <td class="contact1">32340</td>
</tr>
<tr>
  <td class="contact">17</td>
  <td class="contact"><strong>CIOMAS WONOAYU</strong></td>
  <td class="contact"> 031-8973620</td>
  <td class="contact">51201</td>
  <td class="contact">71355</td>
</tr>
<tr>
  <td class="contact1">18</td>
  <td class="contact1"><strong>GREENFIELDS <br>
    GUNUNG KAWI</strong></td>
  <td class="contact1"> 0341-291029</td>
  <td class="contact1">69211</td>
  <td class="contact1">69294 Purchasing, 69295 MP</td>
</tr>
<tr>
  <td class="contact">19</td>
  <td class="contact"><strong>MBAI LAMPUNG</strong></td>
  <td class="contact"> 0721-351227</td>
  <td class="contact">46700</td>
  <td class="contact">46720</td>
</tr>
<tr>
  <td class="contact1">20</td>
  <td class="contact1"><strong>MBAI PURWAKARTA</strong></td>
  <td class="contact1"> 0264-201040</td>
  <td class="contact1">46580</td>
  <td class="contact1">46564</td>
</tr>
<tr>
  <td class="contact">21</td>
  <td class="contact"><strong>SGF MANUFACTURING<br> 
    TANGERANG</strong></td>
  <td class="contact"> 021-59400610</td>
  <td class="contact">61200</td>
  <td class="contact">61199</td>
</tr>
<tr>
  <td class="contact1">22</td>
  <td class="contact1"><strong>SGF CAKUNG</strong></td>
  <td class="contact1"> 021- 468 29108</td>
  <td class="contact1">59500</td>
  <td class="contact1">59508</td>
</tr>
<tr>
  <td class="contact">23</td>
  <td class="contact"><strong>SGF BANDUNG</strong></td>
  <td class="contact"> 022-540 7491</td>
  <td class="contact">59600</td>
  <td class="contact">&nbsp;</td>
</tr>
<tr>
  <td class="contact1">24</td>
  <td class="contact1"><strong>SGF SURABAYA</strong></td>
  <td class="contact1"> 031-328 5042</td>
  <td class="contact1">59700</td>
  <td class="contact1">&nbsp;</td>
</tr>
<tr>
  <td class="contact">25</td>
  <td class="contact"><strong>SGF SEMARANG</strong></td>
  <td class="contact"> 024-866 1970</td>
  <td class="contact">59813</td>
  <td class="contact">59820</td>
</tr>
<tr>
  <td class="contact1">26</td>
  <td class="contact1"><strong>STP BANYUWANGI</strong></td>
  <td class="contact1"> 0333-423 256</td>
  <td class="contact1">56900</td>
  <td class="contact1">56969</td>
</tr>
<tr>
  <td class="contact">27</td>
  <td class="contact"><strong>STP LAMPUNG</strong></td>
  <td class="contact"> 0721-370 202</td>
  <td class="contact">56500</td>
  <td class="contact">56667 Umum, 56668 Pembelian</td>
</tr>
<tr>
  <td class="contact1">28</td>
  <td class="contact1"><strong>SANTORI PROBOLINGGO</strong></td>
  <td class="contact1"> 0335-511239</td>
  <td class="contact1">64300</td>
  <td class="contact1">&nbsp;</td>
</tr>
<tr>
  <td class="contact">29</td>
  <td class="contact"><strong>SANTORI JABUNG LAMPUNG</strong></td>
  <td class="contact"> 0721-482627</td>
  <td class="contact">64700</td>
  <td class="contact">64732</td>
</tr>
<tr>
  <td class="contact1">30</td>
  <td class="contact1"><strong>SANTORI BEKRI LAMPUNG</strong></td>
  <td class="contact1"> 0725-26500</td>
  <td class="contact1">64500</td>
  <td class="contact1">64534</td>
</tr>
<tr>
  <td class="contact">31</td>
  <td class="contact"><strong>VAKSINDO</strong></td>
  <td class="contact"> 021-867 0414</td>
  <td class="contact">34101</td>
  <td class="contact">&nbsp;</td>
</tr>
<tr>
  <td class="contact1">32</td>
  <td class="contact1"><strong>JAPFA SINGAPORE</strong></td>
  <td class="contact1"> (65) 67350031</td>
  <td class="contact1">36011</td>
  <td class="contact1">&nbsp;</td>
</tr>
<tr>
  <td class="contact">33</td>
  <td class="contact"><strong>JCI GROBOGAN</strong></td>
  <td class="contact">(0292) 5135885</td>
  <td class="contact">13200</td>
  <td class="contact">(0292) 5135877</td>
</tr>
<tr>
  <td class="contact1">34</td>
  <td class="contact1"><strong>JCI PURWAKARTA</strong></td>
  <td class="contact1">(0264) 8287100</td>
  <td class="contact1">14100</td>
  <td class="contact1">14398, 14399</td>
</tr>
<tr>
  <td class="contact">35</td>
  <td class="contact"><strong>JCI BREBES</strong></td>
  <td class="contact">&nbsp;</td>
  <td class="contact">14700</td>
  <td class="contact">&nbsp;</td>
</tr>
</table>
