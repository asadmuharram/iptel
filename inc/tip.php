<style>
table.contacts
{ width: 352px;
background-color: #fafafa;
border: 1px #99CCCC solid;
border-collapse: collapse;
border-spacing: 0px; }


td.contactDept
{ background-color: #99CCCC;
border: 1px #99CCCC solid;
font-family: verdana;
font-weight: bold;
font-size: 13px;
text-align:center;
color: #404040; }


td.contact
{ border-bottom: 1px #6699CC dotted;
text-align: left;
font-family: Arial;
font-weight: normal;
font-size: 13px;
color: #404040;
background-color: #fafafa;
padding-top: 4px;
padding-bottom: 8px;
padding-left: 8px;
padding-right: 0px; }

td.contact1
{ border-bottom: 1px #6699CC dotted;
text-align: left;
font-family: Arial;
font-weight: normal;
font-size: 13px;
color: #404040;
background-color: #CDEDED;
padding-top: 4px;
padding-bottom: 4px;
padding-left: 8px;
padding-right: 0px; }

td.contact2
{ border-bottom: 1px #6699CC dotted;
text-align: left;
font-family: Arial;
font-weight: normal;
font-size: 13px;
color: #404040;
background-color: #E7F5F5;
padding-top: 4px;
padding-bottom: 4px;
padding-left: 8px;
padding-right: 0px; }

</style>
<h2>Tips Menghemat Biaya Telpon di Japfa<br />Phone Cost Saving Tips</h2>
<p><a href="files/phone_cost_saving_tipsr3.pdf"><img src="images/pdf.gif" class="floated"> English Version </a></p>
			<p>Di kantor-kantor yang telah terpasang dengan sistem IP Telephony Avaya, panggilan telpon antar lokasi tersebut dapat dilakukan secara langsung dengan memanggil nomor extension.  Panggilan antar extension ini bebas biaya. Selain itu antar kantor ini juga bisa mengirimkan fax ke nomor extension fax di kantor tujuan, sehingga bebas biaya.  Untuk menelpon ke lokasi yang belum terpasang sistem IP Telephony Avaya atau pun menelpon dari tempat yang belum terpasang dengan sistem IP Telephony Avaya, ada beberapa hal yang bisa dilakukan untuk menghemat biaya telpon.

Berikut ini tips menghemat biaya telpon:</p>
			<p><h3>Untuk menelpon rekan di kantor Japfa yang telah terpasang dengan sistem IP Telephony</h3>
		    <ol>
		      <li>
		    Saat berada di lingkungan kantor, lakukan panggilan langsung antar ekstension yang bebas biaya. Hindari melakukan
panggilan menggunakan telpon kantor ke mobile phone ataupun melakukan panggilan antar mobile phone.
  </li>
	          <li> Saat berada di luar kantor dan hendak menelpon extension di kantor, lakukan panggilan ke nomor telpon kantor Japfa
terdekat lalu tekan nomor extension yang dituju atau tekan tanda # untuk bantuan operator. 
                <br />
                Contoh:
                <ul style="list-style-type:circle">
                  <li >User dari kantor Sidoarjo yang sedang melakukan perjalanan di Jakarta (di luar kantor Jakarta) hendak menelpon
extension di Sidoarjo:
Lakukan panggilan ke nomor telpon kantor Jakarta lalu tekan nomor extension yang dituju. Biaya telpon yg
digunakan adalah biaya telpon lokal di Jakarta.</li>
                  <li> User dari kantor Austasia Food - Kallang Singapore yang tidak terhubung dengan sistem IP Telephony hendak
menelpon extension di Greenfields Malang:
Lakukan panggilan ke nomor telpon kantor Japfa Orchard, lalu tekan nomor extension yang dituju atau tekan nomor
extension operator di Greenfields Malang atau menekan tanda # untuk bantuan operator di Japfa Orchard. Biaya
telpon yang digunakan adalah biaya telpon lokal di Singapore.</li>
                  <li> User dari depo SGF sekitar Semarang yang tidak terhubung dengan sistem IP telephony hendak menelpon ke
extension di kantor SGF Jakarta:<br>
Lakukan panggilan ke nomor telpon kantor SGF Semarang, lalu tekan nomor extension yang dituju atau tekan nomor
extension operator di SGF Jakarta atau tekan tanda # untuk bantuan operator di SGF Semarang. Biaya telpon yang
digunakan adalah biaya telpon lokal di Semarang.</li>
                </ul>
	          </li>
		      <li> Bagi pengguna mobile phone yang berada di luar kantor dan hendak menelpon ke kantor, dapat melakukan panggilan ke
nomor selular sbb: <br />
<table cellspacing="0" class="contacts" summary="Contacts template">
  <tr>
    <td class="contactDept"><strong>Operator</strong></td>
    <td class="contactDept"><strong>Nomor</strong></td>
    </tr>
  <tr>
    <td width="30%" rowspan="2" class="contact"><strong>TELKOMSEL </strong></td>
    <td class="contact" width="70%"><strong>08111 206 800</strong></td>
    </tr>
  <tr>
    <td class="contact2"><strong>08111 206 700</strong></td>
    </tr>
  <tr>
    <td class="contact"><strong>XL</strong></td>
    <td class="contact"><strong>08788 55 22 700</strong></td>
    </tr>
  <tr>
    <td class="contact2"><strong>INDOSAT</strong></td>
    <td class="contact2"><strong>0858 10 282 700</strong></td>
    </tr>
  <tr>
    <td class="contact">&nbsp;</td>
    <td class="contact">Catatan: nomor seluler lain sedang dalam proses penambahan.</td>
    </tr>
</table>
		      </li>
	      </ol>
		    <h3>Untuk menelpon ke pihak luar atau ke kantor Japfa yang belum terpasang dengan sistem IP Telephony</h3>
			<ol>
			  <li>
			  Gunakan nomor akses panggilan SLI yang hemat biaya yaitu 01017 + kode Negara + kode area + nomor tujuan. Contoh,
untuk panggilan dari Indonesia ke India, gunakan 01017 + 91 + kode area + no tujuan.
                <br />
                <table cellspacing="0" class="contacts" summary="Contacts template">
                  <tr>
                    <td colspan="2" class="contactDept"><strong>Contoh panggilan SLI</strong></td>
                  </tr>
                  <tr>
                    <td width="19%" class="contact"><strong>01017 +</strong></td>
                    <td width="81%" class="contact2"><strong>Kode Negara + kode area + nomor tujuan</strong></td>
                  </tr>
                  <tr>
                    <td colspan="2" class="contact">Contoh, untuk panggilan dari Indonesia ke India<br />
                    <strong>01017 + 91 + kode area + no tujuan</strong></td>
                  </tr>
                </table><br />
			  </li>
		      <li>Untuk melakukan panggilan telpon ke pihak di luar Japfa yang berada di kota-kota yang terdapat kantor Japfa yang telah
terpasang sistem IP Telephony, gunakan telpon Avaya. Sistem ini telah dikonfigurasi untuk menggunakan jalur telpon
termurah.
                <br />
                Contoh:
                <ol>
                  <li>User di kantor JCI Sragen hendak menelpon ke supplier di Jakarta:<br>
Lakukan panggilan ke supplier menggunakan kode area 021 + no tujuan. Sistem IP Telephony akan mengalihkan
panggilan dari Sragen ke sistem Avaya di Jakarta dan melakukan panggilan ke supplier menggunakan line telpon
kantor Jakarta. Biaya telpon yang digunakan adalah biaya telpon lokal di Jakarta.<br />
<table cellspacing="0" class="contacts" summary="Contacts template">
  <tr>
    <td width="100%" colspan="2" class="contactDept"><strong>Contoh panggilan di luar JAPFA</strong></td>
  </tr>
  <tr>
    <td colspan="2" class="contact">User di kantor JCI Sragen hendak menelpon ke supplier di Jakarta ;<br />
      <strong>kode area 021 + no tujuan</strong></td>
  </tr>
</table><br />
                  </li>
                  <li>User di Greenfields Gn. Kawi, Greenfields Millenia, dan Austasia Food hendak menelpon ke kantor Austasia Food -
Kallang Singapore yang belum terpasang dengan sistem IP Telephony:<br>
Lakukan panggilan ke kantor Austasia Food - Kallang Singapore menggunakan 01017 + 65 + 63963323. Sistem IP
Telephony akan mengalihkan panggilan dari Indonesia ke kantor Japfa Singapore (Orchard) dan melakukan panggilan
ke Austasia Food Kallang menggunakan line telpon kantor Japfa Orchard. Biaya telpon yang digunakan adalah biaya
telpon lokal di Singapore.<br />
<table cellspacing="0" class="contacts" summary="Contacts template">
  <tr>
    <td width="100%" colspan="2" class="contactDept"><strong>Contoh panggilan di luar JAPFA</strong></td>
  </tr>
  <tr>
    <td colspan="2" class="contact">User di Greenfields Gn. Kawi, Greenfields Millenia, dan Austasia Food hendak menelpon ke kantor Austasia Food - Kallang Singapore yang belum terpasang dengan sistem IP Telephony ;<br />
      <br />
      <strong>01017 + 65 + 63963323</strong></td>
  </tr>
</table>
                  </li>
                </ol>
		      </li>
	      </ol>
		          </p>
<h3>Menelpon ke nomor GSM</h3>
                  <p> Saat ini telah terpasang kartu-kartu GSM yang menggunakan nomor-nomor berakhiran dengan angka 680 dalam sistem IP Telephony
Avaya. Dengan terpasangnya kartu-kartu inin, bila kita melakukan panggilan telpon ke nomor GSM (Telkomsel, Indosat, XL) dari IP
Phone Avaya, maka nomor panggil yang tertera di handphone penerima akan berakhiran dengan angka 680. Hal ini bisa
disosialisasikan kepada para pelanggan/relasi agar mereka bisa mengenal nomor panggil kita.</p>
<p>Informasi lebih lanjut dari ditemukan di <a href="http://info.japfacomfeed.co.id/iptel" target="_blank">http://info.japfacomfeed.co.id/iptel</a> . Untuk pertanyaan lebih
lanjut, silahkan hubungi: Voice Admin di ext. <strong>82001</strong> atau email <a href="mailto:voiceadmin@japfacomfeed.co.id">voiceadmin@japfacomfeed.co.id</a></p>

		