<h2>Fasilitas Video Conference</h2>
<p>Saat ini telah terpasang sistem video conference (vicon) dari Polycom di Millenia.<br>
Ada dua cara untuk conference menggunakan sistem ini yaitu :</p>
		    <ol>
		      <li>
		    Menggunakan perangkat Polycom HDX yg terpasang di WM, DM dan SDA
  </li>
	          <li> Menggunakan software CMA Desktop yang memanfaatkan fitur audio dan 
webcam di PC/notebook.
</ol>
		    <h3>Fasilitas video conference ini diharapkan dapat dipergunakan antara lain untuk:</h3>
			<ol>
			  <li>Meeting
  </li>
		      <li>Training</ol>
<p>Karena adanya keterbatasan jumlah lisensi, kami akan membuatkan beberapa 
user account logon CMA desktop untuk tiap unit untuk penggunaan bersama.</p>
<p>
Untuk informasi lebih lanjut mengenai user account dan penggunaan vicon ini, 
silakan kontak Imanuel 
(<a href="mailto:Imanuel@japfacomfeed.co.id">Imanuel@japfacomfeed.co.id</a>) di ext 
82123.</p>
<p>
Semoga fasilitas ini dapat memperlancar komunikasi antar unit, dan juga 
dapat mengurangi frekuensi travelling sehingga dapat menghemat waktu dan 
biaya.</p>
<p>Source progam dapat diambil pada link <a href="ftp://ftp.japfacomfeed.co.id/Polycom/">ftp://ftp.japfacomfeed.co.id/Polycom/</a>.</p>
			  
			  