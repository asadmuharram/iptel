<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>IP Telephony</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<!-- CuFon: Enables smooth pretty custom font rendering. 100% SEO friendly. To disable, remove this section -->
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<!-- CuFon ends -->

<script src="js/jquery.js" type="text/javascript"></script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
     <?php include("inc/menutop.php"); ?>
      <div class="logo"><h1></h1></div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="body">
    <div class="body_resize">
      <div class="left"> 
	   <p>
       <h3>Daftar kantor yg terpasang dgn system IP Telephony<br>List of offices which have been installed with IP Telephony system</h3>
       <p><a href="files/phone_cost_saving_tips.pdf"><img src="images/pdf.gif" class="floated"> English Version </a></p>
       <p><img src="images/tips12-a.png" width="641" height="887" /><br /><img src="images/tips12-b.png" width="641" height="887" /><br /><img src="images/tips12-c.png" width="643" height="508" /></p>
<p>Di luar daftar tersebut di atas, terdapat juga kantor-kantor kecil atau farm dengan 2 extension telpon yang terhubung dengan sistem
IP Telephony Avaya.</p>

       </p>
      </div>
      <div class="right">
       <?php include("inc/menuright.php"); ?> 
      </div>
        <div class="clr"></div>
    </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="FBG">
    <div class="FBG_resize">
     
      <div class="clr"></div>
    </div>
  </div>
  <?php include("inc/footer.php"); ?> 
</div>
</body>
</html>
