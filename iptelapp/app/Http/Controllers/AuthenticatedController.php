<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
Use Sentinel;
Use Session;

class AuthenticatedController extends Controller
{
    public function login()
    {
	return view('auth.login');
    }

    public function postLogin(Request $request)
    {
	if(Sentinel::authenticate($request->all())){
	    return redirect('/');
	} else {
	    flash('Login Invalid', 'warning');
	    return redirect('login');
	}
    }

    public function logout()
    {
        Sentinel::logout();
        return redirect('login');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {	$user = Sentinel::registerAndActivate($request->all());
	dd($user);
    }

}
