<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\User;
use Validator;

class UserController extends Controller
{
     public function index(){
    	  $users = User::users();
	  //print_r($users);
    	  return response()->json(array('data' => $users));
    }

    public function getUserById(Request $request, $id){
    	  $user = User::where('id' , $id)->select('email','first_name','last_name','id')->first();
          if($user === null){
	    return response()->json(['status' => 'data not exist'], 400);
	  } else {
	    return response()->json($user);
	  }
    }

    public function createUser(Request $request){
          $validator = Validator::make($request->all(), [
            'email' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
	    	'role' => 'required',
	    	'password' => 'required|confirmed',
	    	'password_confirmation' => 'required'
    	  ]);
	  if ($validator->fails()) {
	    $errors = $validator->errors();
    	    $errors =  json_decode($errors);
            return response()->json([
		'status' => 'error',
		'message' => $errors
	    ], 400);
          }
	  $credentials = [
    		'email' => $request->input('email'),
		'first_name' => $request->input('first_name'),
		'last_name' => $request->input('last_name'),
		'role' => $request->input('role'),
	        'password' => $request->input('password'),
	  ];
    	  $user = Sentinel::registerAndActivate($credentials);
	  $user = Sentinel::findById($user->id);
	  $role = Sentinel::findRoleByName($request->input('role'));
          $user->roles()->attach($role);
    	  return response()->json(['status' => 'success'], 200);
	}

    public function updateUser(Request $request, $id){
    	if ($request->input('password') == '') {
    		$validator = Validator::make($request->all(), [
	            'email' => 'required',
	            'first_name' => 'required',
	            'last_name' => 'required',
		    	'role' => 'required'
    	  	]);
    	} else {
    		$validator = Validator::make($request->all(), [
	            'email' => 'required',
	            'first_name' => 'required',
	            'last_name' => 'required',
		    	'role' => 'required',
		    	'password' => 'confirmed',
		    	'password_confirmation' => 'required',
    	  	]);
    	}
        if ($validator->fails()) {
	    $errors = $validator->errors();
    	    $errors =  json_decode($errors);
            return response()->json([
		'status' => 'error',
		'message' => $errors
	    ], 400);
          }
	  $credentials = [
    		'email' => $request->input('email'),
		'first_name' => $request->input('first_name'),
		'last_name' => $request->input('last_name'),
		'role' => $request->input('role'),
	        'password' => $request->input('password'),
	  ];
    	  $user = Sentinel::findById($id);
          if($user === null){
	    return response()->json(['status' => 'data not exist'], 400);
	  } else {
	    $user = Sentinel::update($user, $credentials);
	    $userRole = Sentinel::findById($user->id)->roles()->get();
	    $role = Sentinel::findRoleByName($userRole);
	    $user->roles()->detach($role);
	    $role = Sentinel::findRoleByName($request->input('role'));
	    $role->users()->attach($user);
    	    return response()->json(['status' => 'success'], 200);
	  }
	}

    public function deleteUser(Request $request, $id){
    	  $user = Sentinel::findById($id);
          if($user === null){
	    return response()->json(['status' => 'data not exist'], 400);
	  } else {
    	  $user->delete();
          return response()->json(['status' => 'success'], 200);
	  }
    }
}
