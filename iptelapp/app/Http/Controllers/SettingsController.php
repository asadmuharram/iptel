<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\Fileentry;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Form;
use Illuminate\Support\Facades\Input;
use Session;
use View;
use Validator;

class SettingsController extends Controller
{
    public function index()
    {
	return view('admin.setting.index');
    }

    public function users()
    {
	return view('admin.setting.user');
    }

    public function roles()
    {
	$roles = Sentinel::getRoleRepository();
	//dd($roles);
	return view('admin.setting.role');
    }

    public function files()
    {
	$entries = Fileentry::all();
	return view('admin.setting.file', compact('entries'));
    }

    public function resetpassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed'
          ]);
        if ($validator->fails()) {
        $errors = $validator->errors();
        flash($errors, 'danger');
        return redirect('settings');
        };
        $hasher = Sentinel::getHasher();
        $oldPassword = $request->input('old_password');
        $password = $request->input('password');
        $passwordConf = $request->input('password_confirmation');
        $user = Sentinel::getUser();
        if (!$hasher->check($oldPassword, $user->password) || $password != $passwordConf) {
            flash('Password is incorrect', 'danger');
            return redirect('settings');
        }
        Sentinel::update($user, array('password' => $password));
	flash('Password updated', 'success');
        return redirect('settings');
    }
}
