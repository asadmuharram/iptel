<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fileentry;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Sentinel;
use DB;

use Carbon\Carbon;

class FileEntryController extends Controller
{
    	 /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$entries = DB::table('fileentries')->orderBy('created_at', 'desc')->get();
		return view('admin.setting.file', compact('entries'));
	}

	public function getAllFiles(){
		$entries = Fileentry::with('users')->get();
		//$entries = Fileentry::with('user')->get(['id','filename','original_filename','created_at','user.first_name']);
    	  	return response()->json(array('data' => $entries));
    	}
 
	public function add(Request $request)
	{
		$file = $request->file('filefield');
		$extension = $file->getClientOriginalExtension();
		Storage::disk('public')->put($file->getFilename().'.'.$extension,  File::get($file));
		$entry = new Fileentry();
		$entry->user_id = Sentinel::getUser()->id;
		$entry->mime = $file->getClientMimeType();
		$entry->original_filename = $file->getClientOriginalName();
		$entry->filename = $file->getFilename().'.'.$extension;
		$entry->save();
		return redirect('files');
		
	}

	public function get($filename)
	{
	
		$entry = Fileentry::where('filename', '=', $filename)->firstOrFail();
		$file = Storage::disk('public')->get($entry->filename);
		return (new Response($file, 200))->header('Content-Type', $entry->mime);
	}
}
