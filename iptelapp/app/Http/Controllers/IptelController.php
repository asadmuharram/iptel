<?php
 
namespace App\Http\Controllers;
 
use App\Iptel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
 
class IptelController extends Controller{

	public function createIptel(Request $request){
          $validator = Validator::make($request->all(), [
            'no' => 'required|max:10',
            'name' => 'required',
            'address' => 'required',
    	  ]);
	  if ($validator->fails()) {
            return response()->json(['status' => 'error'], 400);
          }
    	  $iptel = Iptel::create($request->all());
    	  return response()->json(['status' => 'success'], 200);
	}

        public function getIptelById(Request $request, $id){
    	  $iptel = Iptel::where('id' , $id)->select('name','no','address','id')->first();
          if($iptel === null){
	    return response()->json(['status' => 'data not exist'], 400);
	  } else {
	    return response()->json($iptel);
	  }
	}
 
	public function updateIptel(Request $request, $id){
	  $validator = Validator::make($request->all(), [
            'no' => 'required|max:10',
            'name' => 'required',
            'address' => 'required',
    	  ]);
          if ($validator->fails()) {
            return response()->json(['status' => 'error'], 400);
          }
    	  $iptel = Iptel::find($id);
          if($iptel === null){
	    return response()->json(['status' => 'data not exist'], 400);
	  } else {
	    $iptel->no = $request->input('no');
    	    $iptel->name = $request->input('name');
    	    $iptel->address = $request->input('address');
    	    $iptel->save();
    	    return response()->json(['status' => 'success'], 200);
	  }
	}

	public function deleteIptel(Request $request, $id){
    	$iptel = Iptel::where('id' , $id)->first();
        if($iptel === null){
	    return response()->json(['status' => 'data not exist'], 400);
	  } else {
    	$iptel->delete();
        return response()->json(['status' => 'success'], 200);
	}
	}

	public function index(){
    	  $avaya  = Iptel::all();
    	  return response()->json(array('data' => $avaya));
	}
}
