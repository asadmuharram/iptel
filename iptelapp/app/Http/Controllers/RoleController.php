<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\Role;
use Validator;

class RoleController extends Controller
{
    public function index(){
    	  $roles = Role::all();
	  //dd($roles);
    	  return response()->json(array('data' => $roles));
    }
    
    public function getRoleById(Request $request, $id){
    	  $role = Role::find($id);
          if($role === null){
	    return response()->json(['status' => 'data not exist'], 400);
	  } else {
	    return response()->json($role);
	  }
    }

    public function updateRole(Request $request,$id){
	 $validator = Validator::make($request->all(), [
            'name' => 'required',
    	  ]);
          if ($validator->fails()) {
	    $errors = $validator->errors();
    	    $errors =  json_decode($errors);
            return response()->json([
		'status' => 'error',
		'message' => $errors
	    ], 400);
          }
    	  $role = Role::find($id);
          if($role === null){
	    return response()->json(['status' => 'data not exist'], 400);
	  } else {
	    $role->name = $request->input('name');
	    $role->save();
    	    return response()->json(['status' => 'success'], 200);
	  }
	}
}
