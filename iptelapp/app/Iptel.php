<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iptel extends Model
{
    protected $table = 'db_avaya_ver_1';
    protected $fillable = ['no', 'name', 'address'];
}
