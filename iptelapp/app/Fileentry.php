<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


class Fileentry extends Eloquent
{

    protected $table = 'fileentries';

    public function users()
    {
	 return $this->belongsTo('App\User', 'user_id');
    }
}
