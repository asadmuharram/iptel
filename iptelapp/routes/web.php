<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => 'admin'], function () {
	Route::get('/','AdminController@index');
	Route::get('settings','SettingsController@index');
	Route::group(['middleware' => 'role'], function () {
		Route::get('register','AuthenticatedController@register');
		Route::post('register','AuthenticatedController@postRegister');
		Route::get('users','SettingsController@users');
		Route::get('roles','SettingsController@roles');
	});
	Route::get('files', 'FileEntryController@index');
	/*Route::get('files/get/{filename}', [
			'as' => 'getentry', 'uses' => 'FileEntryController@get']);*/
	Route::post('files/add',[ 
        	'as' => 'addentry', 'uses' => 'FileEntryController@add']);
	});
	Route::post('settings/resetpassword', array('as' => 'reset.password', 'uses' => 'SettingsController@resetpassword'));
	Route::group(['prefix' => 'api/v1'], function($app)
	{
		/** Iptel API **/
		Route::post('iptel','IptelController@createIptel');
		Route::put('iptel/{id}','IptelController@updateIptel');
		Route::delete('iptel/{id}','IptelController@deleteIptel');
		Route::get('iptel','IptelController@index');
		Route::get('iptel/{id}','IptelController@getIptelById');

		/** Role API **/
		Route::get('roles','RoleController@index');
		Route::get('role/{id}','RoleController@getRoleById');
	        Route::put('role/{id}','RoleController@updateRole');

		/** User API **/
		Route::get('users','UserController@index');
		Route::get('user/{id}','UserController@getUserById');	
	        Route::post('users','UserController@createUser');
	        Route::delete('user/{id}','UserController@deleteUser');
		Route::put('user/{id}','UserController@updateUser');

		/** File API **/
		Route::get('allfiles', 'FileEntryController@getAllFiles');
	});

Route::get('login','AuthenticatedController@login');
Route::post('login','AuthenticatedController@postLogin');
Route::get('logout','AuthenticatedController@logout');


