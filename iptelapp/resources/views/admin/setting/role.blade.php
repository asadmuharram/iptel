@extends('layouts.main')
@section('content')
<h4>JAPFA IP Telephony <small>Home > Settings > Roles</small></h4>
<hr/>
<table class="table table-bordered table-hover" id="tabelRole">
	<thead>
		<tr>
		   <th>#</th>
		   <th>Role Name</th>
		   <th>Action</th>
		</tr>
	</thead>					
</table>
		<div id="createModal" class="modal fade bs-example-modal-sm modal-create" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Add Role</h4>
					</div>
					<div class="modal-body">
						<form id="formInput">
							{{ csrf_field() }}
							<div class="form-group">
								<label>Role Name</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Type role name">
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-success" id="submit">Save Changes</button>
					</div>
				</div></div>
				</div>
		<div id="updateModal" class="modal fade bs-example-modal-sm modal-edit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Edit User</h4>
					</div>
					<div class="modal-body">
						<form id="formInputEditRole">
						{{ csrf_field() }}
						<input type="hidden" id="hiddenIdEditRole" name="id">
						<div class="form-group">
							<label>Role Name</label>
							<input type="text" class="form-control" id="name_role_edit" name="name" placeholder="Type role name">
						</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-success" id="submitEditRole">Save Changes</button>
					</div>
				</div>
			</div>
		</div>
@stop
