@extends('layouts.main')
@section('content')
<h4>JAPFA IP Telephony <small>Home > Settings > Files</small></h4>
<hr/>
<div class="row">
<div class="col-md-12">

</div>
</div>
<div class="row">
<div class="col-md-4">
<div class="panel panel-default">
  <div class="panel-body">
    <form action="files/add" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input class="form-control" type="hidden" name="hiddenId">
	<div class="form-group">
	<label>Upload Files *</label>
        <input class="form-control" type="file" name="filefield" required>
	</div>
	<div class="form-group">
        <input class="btn btn-success" type="submit">
	</div>
    </form>
</div>
</div>
</div>
<div class="col-md-8">
 <h2> Files list</h2>
<hr/>
<table class="table table-bordered table-hover" id="tabelFile">
	<thead>
		<tr>
		   <th>File Name</th>
		   <th>Upload Date</th>
		   <th>Uploaded By</th>
		   <th>Action</th>
		</tr>
	</thead>					
</table>
</div>
</div>
@stop
