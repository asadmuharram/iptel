@extends('layouts.main')
@section('content')
<h4>JAPFA IP Telephony <small>Home > Settings</small></h4>
<hr/>
<h3>Change Password</h3>
@include('flash::message')
<div class="col-md-6">
{{ Form::open(array('route' => array('reset.password'))) }}
  <div class="form-group">
  {{ Form::label('Current Password', null, ['class' => 'control-label']) }}
  {{ Form::password('old_password', array('placeholder'=>'current password', 'required'=>'required' , 'class'=>'form-control')) }}
  </div>
  <div class="form-group">
  {{ Form::label('New Password', null, ['class' => 'control-label']) }}
  {{ Form::password('password', array('placeholder'=>'new password', 'required'=>'required', 'class'=>'form-control')) }}
  </div>
  <div class="form-group">
  {{ Form::password('password_confirmation', array('placeholder'=>'new password confirmation', 'required'=>'required', 'class'=>'form-control')) }}
  </div>
  <div class="form-group">
  {{ Form::submit('Reset Password', array('class' => 'btn btn-success')) }}
  </div>
{{ Form::close() }}
</div>
@stop
