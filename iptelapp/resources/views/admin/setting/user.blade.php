@extends('layouts.main')
@section('content')
<h4>JAPFA IP Telephony <small>Home > Settings > Users</small></h4>
<hr/>
<table class="table table-bordered table-hover" id="tabelUser">
	<thead>
		<tr>
		   <th>#</th>
		   <th>Email</th>
		   <th>First Name</th>
		   <th>Last Name</th>
		   <th>Actions</th>
		</tr>
	</thead>					
</table>
		<div id="createModal" class="modal fade bs-example-modal-sm modal-create" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
				     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Add User</h4>
					</div>
					<div class="modal-body">
						<div id="response"></div>
						<form id="formInputAdd">
							{{ csrf_field() }}
							<div class="form-group">
								<label>Email</label>
								<input type="text" class="form-control" id="email" name="email" placeholder="Type email">
							</div>
							<div class="form-group">
								<label>First Name</label>
							<input type="text" class="form-control" id="first_name" name="first_name" placeholder="Type first name">
							</div>
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" class="form-control" id="name" name="last_name" placeholder="Type last name">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Type password">
							</div>
							<div class="form-group">
								<label>Password Confirmation</label>
								<input type="password" class="form-control" id="password_c" name="password_confirmation" placeholder="Confirm password">
							</div>
							<div class="form-group">
								<label>Role</label>
								<select class="form-control" id="role" name="role" placeholder="Select Role">
									@foreach($roles as $role)
										<option value="{{$role->name}}">{{$role->name}}</option>
									@endforeach
								</select>
							</div>
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-success" id="submitUserAdd">Save Changes</button>
					</div>
					</form>
				</div>
</div>
</div>
		<div id="updateModal" class="modal fade bs-example-modal-sm modal-edit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Edit User</h4>
					</div>
					<div class="modal-body">
						<form id="formInputEdit">
							<input type="hidden" id="hiddenIdEdit" name="idUser">
							{{ csrf_field() }}
							<div class="form-group">
								<label>Email</label>
								<input type="text" class="form-control" id="email_edit" name="email" placeholder="Type role email">
							</div>
							<div class="form-group">
								<label>First Name</label>
								<input type="text" class="form-control" id="first_name_edit" name="first_name" placeholder="Type first name">
							</div>
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" class="form-control" id="last_name_edit" name="last_name" placeholder="Type last name">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Type password">
							</div>
							<div class="form-group">
								<label>Password Confirmation</label>
								<input type="password" class="form-control" id="password_c" name="password_confirmation" placeholder="Confirm password">
							</div>
							<div class="form-group" id="selectRole">
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-success" id="submitEditAdmin">Save Changes</button>
					</div>
					</form>
				</div>
			</div>
		</div>

@stop
