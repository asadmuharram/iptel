@extends('layouts.main')
@section('content')
<h4>JAPFA IP Telephony <small>Home</small></h4>
			<hr/>		
<div class="tab-content">
<div role="tabpanel" class="tab-pane active" id="iplist">
<br/>
					<table class="table table-bordered table-hover" id="tabelIptel">
						<thead>
							<tr>
								<th>#</th>
								<th>Ext No.</th>
								<th>Name</th>
								<th>Address</th>
								<th>Action</th>
							</tr>
						</thead>
						
					</table>
			</div>
		</div>

</div>

</div>
		<div id="updateModal" class="modal fade bs-example-modal-sm modal-edit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Edit Data</h4>
					</div>
					<div class="modal-body">
						<form id="formInputEdit">
							<input type="hidden" id="hiddenIdEdit" name="id">
							{{ csrf_field() }}
							<div class="form-group">
								<label>Ext No. *</label>
								<input type="text" class="form-control" id="extNoEdit" name="no" placeholder="Type Ext No." required>
							</div>
							<div class="form-group">
								<label>Name *</label>
								<input type="text" class="form-control" id="nameEdit" name="name" placeholder="Type Name" required>
							</div>
							<div class="form-group">
								<label>Address *</label>
								<textarea class="form-control" id="addressEdit" name="address" placeholder="Type Address" required></textarea>
							</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-success" id="submitEdit">Save Changes</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div id="createModal" class="modal fade bs-example-modal-sm modal-create" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Create Data</h4>
					</div>
					<div class="modal-body">
						<form id="formInput">
							<input type="hidden" id="hiddenIdEdit" name="idUser">
							{{ csrf_field() }}
							<div class="form-group">
								<label>Ext No. *</label>
								<input type="text" class="form-control" id="extNo" name="no" placeholder="Type Ext No." required>
							</div>
							<div class="form-group">
								<label>Name *</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Type Name" required>
							</div>
							<div class="form-group">
								<label>Address *</label>
								<textarea class="form-control" id="address" name="address" placeholder="Type Address" required></textarea>
							</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success" id="submit">Save Changes</button>
					</div>
					</form>
				</div>

@stop
