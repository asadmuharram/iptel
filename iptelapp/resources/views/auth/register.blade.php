<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Admin IP Telephony</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
		<style type="text/css">
			.top10 {
				margin-top: 10%;
			}
			.btn-space {
    				margin-left: 5px;
			}
		</style>
	</head>
	<body>
		<div class="container top10">
<div class="col-md-6 col-md-offset-3">
<div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="register" method="post">
			    {{ csrf_field() }}
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus="">
                                </div>
				<div class="form-group">
                                    <input class="form-control" placeholder="First Name" name="first_name" type="text" autofocus="">
                                </div>
				<div class="form-group">
                                    <input class="form-control" placeholder="Last Name" name="last_name" type="text" autofocus="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
				<div class="form-group">
                                    <input class="form-control" placeholder="Password Confirmation" name="password_c" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
		    		<input type="submit" class="btn btn-sm btn-success" value="Submit">
                            </fieldset>
                        </form>
                    </div>
                </div>
</div>

</div>	
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="js/jquery.dataTables.min.js"></script>
	              <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	              <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
		<script src="js/notify.min.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>
