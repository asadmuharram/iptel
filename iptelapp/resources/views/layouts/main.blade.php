<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Admin IP Telephony</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="{{url('css/app.css')}}">
		<style type="text/css">
			.top10 {
				margin-top: 2%;
			}
			.btm10 {
				margin-bottom: 2%;
			}
			.btn-space {
    				margin-left: 5px;
			}
		</style>
	</head>
	<body>
		@include('layouts.menu')
		<div class="container top10">
			<div class="col-md-10 col-md-offset-1">
				@yield('content')
			</div>
		</div>	
		@include('layouts.footer')
	</body>
</html>
