<nav class="navbar navbar-default" role="navigation">
    	  <div class="container">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-brand-centered">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a href="{{url('/')}}"><div class="navbar-brand navbar-brand-centered">JAPFA IP Telephony</div></a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="navbar-brand-centered">
		      <ul class="nav navbar-nav">
		        
		      </ul>
		      <ul class="nav navbar-nav navbar-right">
			<li><a href="{{ url('/') }}">Home</a></li>
			<li><a href="files">Files</a></li>
			@if(Sentinel::getUser()->roles()->first()->slug == 'admin')
			<li><a href="users">Users</a></li>
		        <!--<li><a href="roles">Roles</a></li>-->
			@endif	
		        <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hello, {{ ucfirst(Sentinel::getUser()->first_name) }} <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li>
			<li><a href="settings">Settings</a></li>
			<li class="divider"></li>
			<li><a href="logout">Logout</a></li>
	      	    </li>
                  </ul>
                </li>		        
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
