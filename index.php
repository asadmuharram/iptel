<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>IP Telephony</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<!-- CuFon: Enables smooth pretty custom font rendering. 100% SEO friendly. To disable, remove this section -->
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<!-- CuFon ends -->

<script src="js/jquery.js" type="text/javascript"></script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
     <?php include("inc/menutop.php"); ?>
      <div class="logo"><h1></h1></div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="body">
    <div class="body_resize">
      <div class="left">   
      <h2>JAPFA IP Telephony </h2>
       <?php include("inc/img1.php"); ?><p>Saat ini di Japfa group yang telah terpasang sistem PABX berbasis IP Telephony dari vendor Avaya.  Sistem IP Telephony Avaya ini menghubungkan berbagai kantor Japfa yang berada di Indonesia maupun yang berada di Singapore, India, Vietnam, China dan Myanmar.   Dengan terpasangnya sistem ini, maka user di semua lokasi tsb dapat melakukan panggilan telpon langsung antar extension. Panggilan antar extension ini bebas biaya. Selain itu antar kantor juga bisa mengirimkan fax ke nomor extension fax di kantor tujuan, sehingga bebas biaya.<br><br>
         Setiap IP phone Avaya dilengkapi dengan fasilitas &quot;Directory Search&quot; untuk mencari nomor extension.  Panduan penggunaan IP phone Avaya tersedia di <a href="avaya1.php">sini</a>. Selain itu untuk memudahkan pencarian nomor extension telpon, kami sediakan pula fasilitas searching nomor extension di bawah ini.</p>
      
 <?php include("avaya/formsearching.php"); ?> 
      </div>
      <div class="right">
       <?php include("inc/menuright.php"); ?> 
      </div>
        <div class="clr"></div>
    </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="FBG">
    <div class="FBG_resize">
     
      <div class="clr"></div>
    </div>
  </div>
  <?php include("inc/footer.php"); ?> 
</div>
</body>
</html>
