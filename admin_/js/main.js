/*function reloadDataAjax(){
    $.ajax({
    url: 'http://192.168.33.10/iptel/iptelapi/public/api/v1/iptel',
    type: 'GET',
    dataType: 'JSON',
    success: function(data) {
        console.log(data);
        var no = 1;
        $('#tabelIptel').append('<tbody id="dataUser">');
        for (var i = 0; i < data.length; i++) {
            $('#tabelIptel').append('<tr><td>' + no + '</td><td>' + data[i].no + '</td><td>' + data[i].name + '</td><td>' + data[i].address + '</td><td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target=".bs-example-modal-sm" onclick="GetUserById(' + data[i].id + ')">Edit</button> <button type="button" class="btn btn-danger btn-xs del-btn" data-id="' + data[i].id + '" onclick="DeleteUser(' + data[i].id + ')">Delete</button></td></tr>');
            no++;
        }
        $('#tabelUser').append('</tbody>');
    }
})
}*/
function GetUserById(id) {
    $.ajax({
        url: 'http://192.168.33.10/iptel/iptelapi/public/api/v1/iptel/' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function(data) {
	    console.log(data.no);
	    $("#extNoEdit").val(data.no);
	    $("#nameEdit").val(data.name);
            $("#addressEdit").val(data.address);
            $("#hiddenIdEdit").val(data.id);
        }
    })
}

$('#submitEdit').click(function() {
    var conf = confirm("Yakin untuk merubah ?");
    if (conf == true) {
        var dataJson = $('#formInputEdit').serialize();
	var id = $('#hiddenIdEdit').val();
        $.ajax({
            type: 'PUT',
            url: 'http://192.168.33.10/iptel/iptelapi/public/api/v1/iptel/' + id,
            dataType: 'html',
            cache: false,
            data: dataJson,
            success: function(response) {
		var data = JSON.parse(response);
		console.log(data.status);
                if (data.status == "success") {
                    $('#dataUser').remove();
                    $('#updateModal').modal('hide');
		    $.notify("Data Updated", "success" , { showDuration: 400});
		    $('#tabelIptel').DataTable().ajax.reload(null, false);
                } else {
                    alert("Gagal");
                }
            }
        })
    }
})

function DeleteUser(id) {
    var conf = confirm("Yakin untuk menghapus ?");
    if (conf == true) {
        $.ajax({
            type: 'DELETE',
            url: 'http://192.168.33.10/iptel/iptelapi/public/api/v1/iptel/' + id,
            dataType: 'html',
            cache: false,
            data: {
                id: id
            },
            success: function(response) {
                var data = JSON.parse(response);
		console.log(data.status);
                if (data.status == "success") {
                    $.notify("Data Deleted", "success" , { showDuration: 400});
		    $('#tabelIptel').DataTable().ajax.reload(null, false);
                } else {
                    alert("Gagal");
                }
            }
        })
    }
}
$(document).ready(function() {
    /*reloadDataAjax();*/
    var t = $('#tabelIptel').DataTable( {
	'initComplete': function (oSettings) {
		$('.dataTables_filter').each(function () {
			$(this).append('<button class="btn btn-success btn-sm btn-space pull-right" data-toggle="modal" data-target=".modal-create" type="button">Add New</button>');
		});
	},
    	"ajax": { 
            "url": "http://192.168.33.10/iptel/iptelapi/public/api/v1/iptel",
            "dataSrc": "data" 
        },
	"columnDefs": [
	    {
                "targets": 4,
                "searchable": false,
		"data": "id",
                "render": function ( data, type, full, meta ) {
      			return '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-id="'  + data + '" data-target=".modal-edit" onclick="GetUserById(' + data + ')">Edit</button> <button type="button" class="btn btn-danger btn-xs del-btn" data-id="' + data + '" onclick="DeleteUser(' + data + ')">Delete</button>';
    		},
            },
	],
        "columns": [
           { "data": "id" },
           { "data": "no" },
           { "data": "name" },
           { "data": "address" },
         ],
        "order": [[ 1, 'asc' ]]
    } );

    /*t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/

    $('#submit').click(function() {
        var dataJson = $('#formInput').serialize();
        $.ajax({
            type: 'POST',
            url: 'http://192.168.33.10/iptel/iptelapi/public/api/v1/iptel',
            dataType: 'html',
            cache: false,
            data: dataJson,
            success: function(response) {
		var data = JSON.parse(response);
		console.log(data.status);
                if (data.status == "success" ) {
                    $('#extNo').val('');
                    $('#name').val('');
                    $('#address').val('');
                    $('#createModal').modal('hide');
		    $.notify("Data saved", "success" , { showDuration: 400});
		    $('#tabelIptel').DataTable().ajax.reload();
                } else {
                    alert("Gagal");
                }
            }
        })
    })
});
