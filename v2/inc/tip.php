<h2>Tips Menghemat Biaya Telpon di Japfa</h2>
			<p>Tulisan ini berisi tips untuk menghemat biaya telpon di Japfa. Saat ini (per Juni 2011) terdapat 32 kantor/lokasi dalam Japfa
group yang telah terhubung dalam sistem IP Telephony menggunakan Avaya. 31 kantor berada di Indonesia, sedangkan 1
kantor berlokasi di Orchard Tower - Singapore. Dengan terpasangnya sistem ini, maka user di 32 lokasi tsb dapat melakukan
panggilan langsung antar extension. Panggilan antar extension ini bebas biaya. Di bagian akhir tulisan ini, kami sajikan daftar
kantor yg telah terhubung dalam sistem IP Telephony, nomor telpon kantor tersebut dan nomor extension operator di kantor
tsb. Selain panggilan langsung antar extension, terdapat beberapa cara lain untuk menghemat biaya telpon. Berikut ini tips
menghemat biaya telpon:</p>
			<p><strong>Untuk menelpon rekan di kantor Japfa yang telah terpasang dengan sistem IP Telephony</strong>
		    <ol>
		      <li>
		    Saat berada di lingkungan kantor, lakukan panggilan langsung antar ekstension yang bebas biaya. Hindari melakukan
  panggilan menggunakan telpon kantor ke mobile phone ataupun melakukan panggilan antar mobile phone.
  </li>
	          <li> Saat berada di luar kantor dan hendak menelpon extension di kantor, lakukan panggilan ke nomor telpon kantor Japfa
    terdekat lalu tekan nomor extension yang dituju atau tekan tanda # untuk bantuan operator.
                <br />
                Contoh:
                <ul>
                  <li>User dari kantor Sidoarjo yang sedang melakukan perjalanan di Jakarta (di luar kantor Jakarta) hendak menelpon
              extension di Sidoarjo:
              Lakukan panggilan ke nomor telpon kantor Jakarta lalu tekan nomor extension yang dituju. Biaya telpon yg
              digunakan adalah biaya telpon lokal di Jakarta.              </li>
                  <li> User dari kantor Austasia Food - Kallang Singapore yang tidak terhubung dengan sistem IP Telephony hendak
                menelpon extension di Greenfields Malang:
                Lakukan panggilan ke nomor telpon kantor Japfa Orchard, lalu tekan nomor extension yang dituju atau tekan nomor
                extension operator di Greenfields Malang atau menekan tanda # untuk bantuan operator di Japfa Orchard. Biaya
                telpon yang digunakan adalah biaya telpon lokal di Singapore.                </li>
                  <li> User dari depo SGF sekitar Semarang yang tidak terhubung dengan sistem IP telephony hendak menelpon ke
                  extension di kantor SGF Jakarta:
                  Lakukan panggilan ke nomor telpon kantor SGF Semarang, lalu tekan nomor extension yang dituju atau tekan nomor
                  extension operator di SGF Jakarta atau tekan tanda # untuk bantuan operator di SGF Semarang. Biaya telpon yang
                  digunakan adalah biaya telpon lokal di Semarang. </li>
                </ul>
	          </li>
		      <li> Bagi pengguna mobile phone yang berada di luar kantor dan hendak menelpon ke kantor, dapat melakukan panggilan ke
          nomor selular sbb: 08121210787, 08121210788 (Telkomsel). Catatan: nomor seluler lain sedang dalam proses
          penambahan. </li>
	      </ol>
		    <strong>Untuk menelpon ke pihak luar atau ke kantor Japfa yang belum terpasang dengan sistem IP Telephony</strong>
			<ol>
			  <li>
			  Gunakan nomor akses panggilan SLI yang hemat biaya yaitu 01017 + kode Negara + kode area + nomor tujuan. Contoh,
  untuk panggilan dari Indonesia ke India, gunakan 01017 + 91 + kode area + no tujuan.
  </li>
		      <li>Untuk melakukan panggilan telpon ke pihak di luar Japfa yang berada di kota-kota yang terdapat kantor Japfa yang telah
    terpasang sistem IP Telephony, gunakan telpon Avaya. Sistem ini telah dikonfigurasi untuk menggunakan jalur telpon
    termurah.
                <br />
                Contoh:
                <ol>
                  <li>User di kantor JCI Sragen hendak menelpon ke supplier di Jakarta:
                  Lakukan panggilan ke supplier menggunakan kode area 021 + no tujuan. Sistem IP Telephony akan mengalihkan
                  panggilan dari Sragen ke sistem Avaya di Jakarta dan melakukan panggilan ke supplier menggunakan line telpon
                  kantor Jakarta. Biaya telpon yang digunakan adalah biaya telpon lokal di Jakarta. </li>
                  <li>User di Greenfields Gn. Kawi, Greenfields Millenia, dan Austasia Food hendak menelpon ke kantor Austasia Food -
                    Kallang Singapore yang belum terpasang dengan sistem IP Telephony:
                    Lakukan panggilan ke kantor Austasia Food - Kallang Singapore menggunakan 01017 + 65 + 63963323. Sistem IP
                    Telephony akan mengalihkan panggilan dari Indonesia ke kantor Japfa Singapore (Orchard) dan melakukan panggilan
                    ke Austasia Food Kallang menggunakan line telpon kantor Japfa Orchard. Biaya telpon yang digunakan adalah biaya
                    telpon lokal di Singapore.</li>
                </ol>
		      </li>
	      </ol>
		          </p>
		          <p><a href="files/Phone%20Cost%20Saving%20Tips%20in%20Japfa.pdf">English Version Download</a></p>
		