<!DOCTYPE html>
<!-- saved from url=(0054)http://getbootstrap.com/examples/sticky-footer-navbar/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>IP Telephony</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/easy-autocomplete.min.css" rel="stylesheet" type="text/css" />
    <link href="css/easy-autocomplete.themes.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-red navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://getbootstrap.com/examples/sticky-footer-navbar/#"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="">Home</a></li>
            <li><a href="">Tips</a></li>
            <li><a href="">List of Ext</a></li>
	    <li><a href="">Hotline</a></li>
            <li><a href="">Softphone</a></li>
	    <li><a href="">Video Confrence</a></li>
            <!--<li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="">Action</a></li>
                <li><a href="">Another action</a></li>
                <li><a href="">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="">Separated link</a></li>
                <li><a href="">One more separated link</a></li>
              </ul>
            </li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>	
    <!-- Begin page content -->
    <div class="container-fluid">
      <div class="page-header">
        <div class="top-banner"></div>
      </div>
</div>

    <div class="container">
      <div class="col-md-8">
        <div class="jumbotron">
         <h3>JAPFA IP Telephony Directory Searching</h3>
	 <div class="input-group" id="adv-search">
                <input type="text" class="form-control eac-square input" id="search" placeholder="Type keyword . . ." />
                <div class="input-group-btn">
                    <div class="btn-group" role="group">
                        <!--<div class="dropdown dropdown-lg">
         <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label for="filter">Filter by</label>
                                    <select class="form-control" id="filter">
                                        <option value="no">Ext Number</option>
                                        <option value="name">Name</option>
                                    </select>
                                  </div>
                                  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                </form>
                            </div>-->
                        </div>
                        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>
          </div>
</div>

      </div>
      <div class="col-md-4">
      </div>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="text-muted"><a href="ext.php">Daftar Nomor Ext</a> &nbsp; | &nbsp;<a href="tips.php">Tips Menghemat Biaya Telpon </a> &nbsp; | &nbsp;<a href="avaya1.php">Jenis Pesawat Telp Avaya</a> &nbsp; | &nbsp;<a href="tab.php">Daftar kantor yg terpasang dgn sistem IP Telephony</a>
    &nbsp; | &nbsp;<a href="hot.php">Hot Line</a>
	<br>
    &copy; 2011<a href="http://info.japfacomfeed.co.id"> info.japfacomfeed.co.id</a></p>
      </div>
    </footer>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="jquery.easy-autocomplete.min.js"></script>
    <script>
$(function() {
var options = {
  url: "http://192.168.33.10/iptel/iptelapi/public/api/v1/iptel",
  getValue: "name",
  list: {	
    match: {
      enabled: true
    }
  }
};
$("#search").easyAutocomplete(options);
});
</script>
</body>
</html>
