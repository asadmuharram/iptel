<style type="text/css">
    table {
    width:90%;
    border-top:1px solid #e5eff8;
    border-right:1px solid #e5eff8;
    margin:1em auto;
    border-collapse:collapse;
    }
    td {
    color: #333;
    border-bottom:1px solid #e5eff8;
    border-left:1px solid #e5eff8;
    padding:.3em 1em;
    text-align:center;
    }
h2 {
    font: bold 20px Arial, Helvetica;
    color: #666;
    text-transform: capitalize;    
  
}	
.form-wrapper {
    width: 450px;
    padding: 8px;
    margin: 2px auto;
    overflow: hidden;
    border-width: 1px;
    border-style: solid;
    border-color: #dedede #bababa #aaa #bababa; 
    background-color: #f6f6f6;
    background-image: linear-gradient(top, #f6f6f6, #eae8e8);
}

.form-wrapper #search {
    width: 330px;
    height: 30px;
    padding: 2px 5px;
    float: left;    
    font: 12px 'lucida sans', 'trebuchet MS', 'Tahoma';
    border: 1px solid #ccc;
    box-shadow: 0 1px 1px #ddd inset, 0 1px 0 #fff;
    border-radius: 1px;      
}

.form-wrapper #search:focus {
    outline: 0; 
    border-color: #aaa;
    box-shadow: 0 1px 1px #bbb inset;  
}

.form-wrapper #search::-webkit-input-placeholder {
   color: #999;
   font-weight: normal;
}

.form-wrapper #search:-moz-placeholder {
    color: #999;
    font-weight: normal;
}

.form-wrapper #search:-ms-input-placeholder {
    color: #999;
    font-weight: normal;
} 

.form-wrapper #submit {
    float: right;    
    border: 1px solid #00748f;
    height: 32px;
    width: 100px;
    padding: 0;
    cursor: pointer;
    font: bold 15px Arial, Helvetica;
    color: #fafafa;
    text-transform: uppercase;    
    background-color: #FAAA03;
    background-image: linear-gradient(top, #31b2c3, #0483a0);
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;      
    text-shadow: 0 1px 0 rgba(0, 0 ,0, .3);
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.3) inset, 0 1px 0 #fff;
}

p {
    font: bold 25px Arial, Helvetica;
    color: #666;
    text-transform: capitalize;    
  
}
  
.form-wrapper #submit:hover,
.form-wrapper #submit:focus {       
    background-color: #31b2c3;
    background-image: linear-gradient(top, #0483a0, #31b2c3);
}   
  
.form-wrapper #submit:active {
    outline: 0;    
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;    
}
  
.form-wrapper #submit::-moz-focus-inner {
    border: 0;
}
</style>

<form class="form-wrapper" name="formcari" method="post" action="index-av1.php">
    <input type="text" name="name" id="search" placeholder="Search for Name..." required>
    <input type="submit" value="search" name="SUBMIT" id="submit">
</form>

<?php
include "connect.php";
$name= $_POST['name']; //get the nama value from form
$q = "SELECT * from tb_avaya_ok where name like '%$name%' "; //query to get the search result
$result = mysql_query($q); //execute the query $q
echo "<center>";
echo "<h2> Hasil Searching </h2>";
echo "<table border='0' cellpadding='5' cellspacing='1'>";
echo "
<tr >
<td><p><b>Nomor Ext Avaya</b></p></td>
<td><p><b>Nama</b></p></td>
<td><p><b>Keterangan</b></p></td>
</tr>";
while ($data = mysql_fetch_array($result)) {  //fetch the result from query into an array
echo "
<tr>
<td>".$data['no']."</td>
<td>".$data['name']."</td>
<td>".$data['address']."</td>
</tr>";
}
echo "</table>";
?>