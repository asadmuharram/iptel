<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>IP Telephony</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="css/easy-autocomplete.min.css" rel="stylesheet" type="text/css" />
<link href="css/easy-autocomplete.themes.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<!-- CuFon: Enables smooth pretty custom font rendering. 100% SEO friendly. To disable, remove this section -->
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<!-- CuFon ends -->

<script src="js/jquery.js" type="text/javascript"></script>
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="jquery.easy-autocomplete.min.js"></script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
     <?php include("inc/menutop.php"); ?>
      <div class="logo"><h1></h1></div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="body">
    <div class="body_resize">
      <div class="left"> 
      <?php include("inc/ex.php"); ?>
       <?php include("avaya/search_exe.php"); ?><br>
	   
      </div>
      <div class="right">
       <?php include("inc/menuright.php"); ?> 
      </div>
        <div class="clr"></div>
    </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="FBG">
    <div class="FBG_resize">
     
      <div class="clr"></div>
    </div>
  </div>
  <?php include("inc/footer.php"); ?> 
</div>
<script>
$( function() {
var options = {
  url: "http://192.168.33.10/iptel/iptelapi/public/api/v1/iptel",
  getValue: "name",
  list: {	
    match: {
      enabled: true
    }
  },
  theme: "square",
};
$("#search").easyAutocomplete(options);
});
</script>
</body>
</html>
