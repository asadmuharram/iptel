<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->singleton('filesystem', function ($app) {
    return $app->loadComponent('filesystems', 'Illuminate\Filesystem\FilesystemServiceProvider', 'filesystem');
});

$app->group(['prefix' => 'api/v1'], function($app)
{
	$app->post('iptel','IptelController@createIptel');
	$app->put('iptel/{id}','IptelController@updateIptel');
	$app->delete('iptel/{id}','IptelController@deleteIptel');
	$app->get('iptel','IptelController@index');
	$app->get('iptel/{id}','IptelController@getIptelById');

	$app->post('file', 'FileController@saveFile');
	$app->get('list', 'FileController@getFileList');
	$app->get('view/{filename}', 'FileController@viewFile');
	$app->get('delete/{filename}', 'FileController@deleteFile');
});
